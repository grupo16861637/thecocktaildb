document.getElementById('llenar').addEventListener('click', () => {
        var alcohol = document.getElementById('alcoholic').checked;
        var noalcohol = document.getElementById('no_alcoholic').checked;    
        var type = alcohol ? 'Alcoholic' : 'Non_Alcoholic';
        var url = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=${type}`;
        
        if (!alcohol && !noalcohol) {
            alert("No has seleccionado nada");
            return;
        }    
        fetch(url)
        .then(response => response.json())
        .then(data => {
            displayCocktails(data.drinks);
            console.log(data);
        })
        .catch(error => {
            console.log("Ha ocurrido un error: ", error);
            alert("No se pudo cargar la lista de cócteles.");
        });
    });
    
   
var totalAlcohol = 0;    
function displayCocktails(cocktails) {
        var cocktailLista = document.getElementById('cocktailLista');
        cocktailLista.innerHTML = '';
    
        if (cocktails) {
            cocktails.forEach(cocktail => {
                var cocktailName = cocktail.strDrink;
                var cocktailImage = cocktail.strDrinkThumb;
    
                var listItem = document.createElement('div');
                listItem.innerHTML = `<h3>${cocktailName}</h3><img src="${cocktailImage}" alt="${cocktailName}">`;
                cocktailLista.appendChild(listItem);
    
                if (cocktail.strAlcoholic != 'Alcoholic') {
                    totalAlcohol++;
                    document.getElementById('cantidad').innerText = "Cantidad de Cocteles: "+totalAlcohol;
                }
            });
        } else {
            cocktailLista.innerHTML = '<p>No se encontraron cócteles.</p>';
        }
        totalAlcohol=0;
    }
    
    
    document.getElementById('clean').addEventListener('click', () => {
        document.getElementById('alcoholic').checked = false;
        document.getElementById('no_alcoholic').checked = false;
        document.getElementById('cocktailLista').innerHTML = '';
        document.getElementById('cantidad').innerHTML="";
    });










